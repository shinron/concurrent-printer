defmodule ConcurrentPrinterTest do
  use ExUnit.Case
  doctest ConcurrentPrinter

  test "check for ConcurrentPrinter receiving the task" do
    assert ConcurrentPrinter.print("Hello") == :ok

    assert ConcurrentPrinter.print("World!") == :ok

    assert ConcurrentPrinter.print("Hello World!") == :ok
  end
end
