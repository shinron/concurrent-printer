# Problem Statement
## Concurrent Printer:
- You have to write Mix app which prints value given by user
- Current limitation is Printer takes 4 sec to print any given text
- App should allow multiple print request (input text to print) at a time
## Acceptance Criteria:
- App service should wait 4 sec before printing
- Allow multiple request
- Run concurrently
## Example:
- ConcurrentPrinter.print("Hello")
> :ok
- ConcurrentPrinter.print("World")
> :ok
> Hello
> World
Note: Above two output must be printed in less than 5 sec as its concurrent

# Instructions:
- Clone the repo.
- Execute the command `iex -S mix`.
