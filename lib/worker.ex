defmodule ConcurrentPrinter.Worker do
  use GenServer

# Client

  def start_link(_), do: GenServer.start_link(__MODULE__, nil)


# Server (callbacks)

  @impl true
  def init(_), do: {:ok, nil}

  @impl true
  def handle_cast({:print, msg}, state) do
    Process.sleep(4000)
    IO.inspect(msg)
    {:noreply, msg}
  end
end
