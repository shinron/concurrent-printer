defmodule ConcurrentPrinter do

  def print(msg) do
    :poolboy.transaction(
      :concurrent_printer, fn pid
        -> GenServer.cast(pid, {:print, msg}
        )
    end)
  end
end
