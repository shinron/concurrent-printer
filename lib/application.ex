defmodule ConcurrentPrinter.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      :poolboy.child_spec(:concurrent_printer, poolboy_config())
    ]

    opts = [strategy: :one_for_one, name: ConcurrentPrinter.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp poolboy_config do
    [
      name: {:local, :concurrent_printer},
      worker_module: ConcurrentPrinter.Worker,
      size: 10,
      max_overflow: 5,
      strategy: :fifo
    ]
  end
end
